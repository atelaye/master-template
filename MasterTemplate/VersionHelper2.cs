﻿using System;
using System.IO;
using System.Web.Hosting;
using System.Xml;

namespace MasterTemplate
{
    public class VersionInfo
    {
        public string BuildVersion { get; set; }
        public string BuildPlan { get; set; }
        public string BuildStatus { get; set; }
        public string BuildDate { get; set; }
        public string DaysAgo { get; set; }
    }

    public class VersionHelper2
    {
        public VersionInfo GetVersionInfo()
        {
            var versionInfo = new VersionInfo();
            string buildPlan = "", buildNumber = "", buildStatus = "", buildDate = "", daysAgo = "", longBuildNumber = "'";

            try
            {
                ReadBuldFile(ref buildPlan, ref buildNumber, ref longBuildNumber);
                GetBuildStatus(buildPlan + buildNumber, ref buildStatus, ref buildDate, ref daysAgo);

                versionInfo.BuildPlan = longBuildNumber;
                versionInfo.BuildVersion = buildNumber;
                versionInfo.BuildStatus = buildStatus;
                versionInfo.BuildDate = buildDate;
                versionInfo.DaysAgo = daysAgo;

                return versionInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void ReadBuldFile(ref string buildPlan, ref string buildNumber, ref string longPlanKey)
        {
            var sr = new StreamReader(new FileStream(HostingEnvironment.MapPath("~/Content/build.txt"), FileMode.Open));

            while (!sr.EndOfStream)
            {
                string temp = sr.ReadLine();
                if (temp.StartsWith("build.key"))
                {
                    temp = temp.Split('=')[1].Trim();
                    longPlanKey = temp;
                    temp = temp.Remove(temp.LastIndexOf('-') + 1);
                    buildPlan = temp;
                }
                else if (temp.StartsWith("build.number"))
                {
                    buildNumber = temp.Split('=')[1].Trim();
                }
            }
        }

        private void GetBuildStatus(string url, ref string buildStatus, ref string buildDate, ref string daysAgo) //Gets the build status from bamboo
        {
            url = "http://build.hcmis.org/rest/api/latest/result/" + url + "/?os_authType=basic&os_username=viewer&os_password=viewer";
            try
            {
                var reader = XmlReader.Create(url);
                reader.ReadToFollowing("result");
                reader.MoveToAttribute("state");
                var state = reader.Value;

                reader.ReadToFollowing("buildCompletedTime");
                buildDate = reader.ReadElementContentAsString();
                daysAgo = GetDaysAgo(DateTime.Parse(buildDate));
                buildStatus = state == "Successful" ? "S" : "F";

            }
            catch (Exception ex)
            {
                buildStatus = "U";
                buildDate = "Unkown";
                daysAgo = "NA";
            }
        }

        private string GetDaysAgo(DateTime d)
        {
            var ts = DateTime.Now.Subtract(d);
            string temp = "";
            if (ts.Days > 0)
                temp = ts.Days + " days ago";
            else if (ts.Hours > 0)
                temp = ts.Hours + " hours ago";
            else if (ts.Minutes > 0)
                temp = ts.Minutes + " minutes ago";
            else if (ts.Seconds > 0)
                temp = ts.Seconds + " Seconds ago";
            else if (ts.Seconds == 0)
                temp = " now";
            return temp;
        }
    }
}